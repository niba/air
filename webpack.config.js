const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const autoprefixer = require('autoprefixer');

function isProduction() {
  return (process.env.NODE_ENV === 'production');
}

function generateEntrySources(sources) {
  if (!isProduction()) sources.push('webpack-hot-middleware/client?path=/__webpack_hmr');
  return sources;
}

const webpackConfig = {
  context: __dirname,
  entry: {
    spren: generateEntrySources([
      path.join(__dirname, 'src', 'docs', 'index.tsx')
    ])
  },
  devtool: !isProduction() ? '#source-map' : null,
  output: {
    path: path.join(__dirname, '/dist/'),
    publicPath: '/',
    filename: 'bundle.js'
  },
  resolve: {
    extensions: ['', '.ts', '.tsx', '.js', '.scss', '.json'],
    modulesDirectories: [
      'node_modules'
    ]
  },
  module: {
    loaders: [
      {
        test: /\.(scss|css)$/,
        loader: ExtractTextPlugin.extract('style', 'css?sourceMap&modules&importLoaders=1&localIdentName=[name]__[local]___[hash:base64:5]!postcss!sass?sourceMap')
      },
      {
        test: /\.tsx?$/,
        exclude: /(node_modules)/,
        include: path.join(__dirname, 'src'),
        loader: 'react-hot!ts-loader'
      },
      {
        test: /\.html$/,
        loader: 'html-loader'
      }
    ]
  },
  postcss: [autoprefixer],
  plugins: [
    new ExtractTextPlugin('bundle.css', { allChunks: true }),
    new webpack.optimize.OccurenceOrderPlugin(),
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoErrorsPlugin(),
    new HtmlWebpackPlugin({
      template: path.join(__dirname, 'src', 'docs', 'index.html'),
      inject: true
    })
  ]
};

module.exports = webpackConfig;
