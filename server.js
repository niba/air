const express = require("express");
const path = require("path");

const app = express();
const port = (process.env.PORT || 3000);

const isDevelopment = (process.env.HOT === "true");

if (isDevelopment) {
  const webpack = require("webpack");
  const webpackDevMiddleware = require("webpack-dev-middleware");
  const webpackHotMiddleware = require("webpack-hot-middleware");
  const config = require("./webpack.config");
  const compiler = webpack(config);
  app.use(webpackDevMiddleware(compiler, { noInfo: true, publicPath: config.output.publicPath }));
  app.use(webpackHotMiddleware(compiler, {
    log: console.log, path: "/__webpack_hmr", heartbeat: 10 * 1000
  }));
} else {
  app.use(express.static(path.join(__dirname, "dist")));
}

app.get("/*", function(req, res) {
  res.sendFile(path.join(__dirname , "dist", "index.html"));
});

app.listen(port, "127.0.0.1", function(error) {
  if (error) {
    console.error(error);
  } else {
    console.info("==> 🌎  Listening on port %s. Open up http://localhost:%s/ in your browser.", port, port);
  }
});
