import * as React from "react";

import FontIcon from "../components/fontIcon/fontIcon.tsx";
import Button from "../components/buttons/button.tsx";
const componentStyles = require('./main.scss');

export default class Main extends React.Component<any, any> {

  getStyles() {
    return {
      main: {
        top: "150px",
        position: "fixed"
      }
    };
  }

  render() {
    const styles = this.getStyles();
    return (
      <div style={styles.main}>
          <h2> Font Icon </h2>
          <FontIcon fontClass={componentStyles.customFont} value="home"/>
          <h2> Buttons </h2>
          <Button value="test" color={RED}/>
      </div>
    );
  };
}
