import * as React from 'react';
import classNames = require('classnames')

const styles = require("./fontIcon.scss");

interface FontIconProps {
  value?: string;
  fontClass?: string;
  children?: any
}

const FontIcon = (props: FontIconProps) => {
  const classes = classNames(
    {'material-icons': typeof props.value === 'string'},
    styles.fontIcon,
    props.fontClass
  );

  return (
    <span className={classes}>
      {props.value}
      {props.children}
    </span>
  )
}

export default FontIcon