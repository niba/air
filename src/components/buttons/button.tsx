import * as React from 'react';
import classNames = require('classnames')

const styles = require("./buttons.scss");

interface ButtonProps {
  value?: string;
  children?: any;
  color?: Color;
}

enum Color {
    BLUE,
    RED,
    ORANGE
}

const Button = (props: ButtonProps) => {
  const value = props.value;
  const classes = classNames(
    {'material-icons': typeof props.value === 'string'},
    styles.button
  );

   return React.createElement('button', { className: classes },
      null,
      value
    );
}

export default Button